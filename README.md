###LDAP Two Steps Login

<br><br><br>
This modules allows a two-steps login by first asking for a username or a mail address then a password if a match is found in Drupal DB or any configured LDAP.

####What this module does:
* Add a hook to the login form in order to insert a class-based display
* Grab the user input and call a the "/check_user_mail" url through ajax
* Look a match inside Drupal DB for a username or a mail address
* If no result, look all configured LDAP servers
* Check through the whitelis and blacklist to ensure thant only one user match

<br><br><br>
####To be implemented:
*	Some configuration: username only / mail adress only / both (default)
*	Security: 
	* link each attempt to the flood table, to avoid brute force logins
	* allow to configure a max attempts number
