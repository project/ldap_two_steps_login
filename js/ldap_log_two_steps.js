/**
 * Created by robin.gueneley on 13/07/2017.
 */
function next_step(){
    jQuery("#user-login").addClass("load-step");
    var user_mail = jQuery("#text-input-user").val();
    jQuery.ajax({
        url : Drupal.settings.basePath + 'check_user_mail',
        type : 'POST',
        data : 'user_mail=' + user_mail,
        dataType : 'json',
        success : function(result, statut){
            if(result["status"] === "failure"){
                jQuery("#messages .section").html('<div class="error messages">' + result["message"] + '</div>');
                jQuery("#user-login").removeClass("load-step");
            }
            else{
                jQuery("#text-input-user").val(result["username"]);
                jQuery("#user-login").removeClass("load-step");
                jQuery("#user-login").attr("class","second-step");
            }
        },
        error : function(res, statut, erreur){
            alert(erreur);
        }
    });
}

function login(){
    jQuery("#user-login").addClass("load-step");
    jQuery("#login-button-submit").click();
}

function retour(){
    jQuery("#user-login").attr("class","first-step");
}

jQuery(document).ready(function(){
    jQuery(window).keydown(function(e){
        if(e.which == 13 && jQuery("#user-login").hasClass("load-step")){
            e.preventDefault();
        }
        else if(e.which == 13 && jQuery("#user-login").hasClass("first-step")){
            e.preventDefault();
            next_step();
        }
        else if(e.which == 13 && jQuery("#user-login").hasClass("second-step")){
            e.preventDefault();
            login();
        }
    });
});
